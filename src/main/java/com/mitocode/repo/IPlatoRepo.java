package com.mitocode.repo;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mitocode.model.Plato;

import reactor.core.publisher.Flux;

public interface IPlatoRepo extends IGenericRepo<Plato, String>{

	/*@Query("find().skip(:page).limit(:size)")
	Flux<Plato> getPagina(@Param("page") int page, @Param("size") int size);*/
}
