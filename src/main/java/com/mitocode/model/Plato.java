package com.mitocode.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Datos para documento Plato")
@Document(collection = "platos")
public class Plato {

	@Id
	private String id;

	@ApiModelProperty(value = "Longitud minima debe ser 3")
	@NotEmpty
	@Field(name = "nombre")
	private String nombre;

	private Double precio;

	@NotNull
	@Field(name = "estado")
	private Boolean estado;

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Plato [id=" + id + "]";
	}
	
	public Plato() {}

	public Plato(String id, @NotEmpty String nombre, Double precio, @NotNull Boolean estado) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.precio = precio;
		this.estado = estado;
	}

	
}
